package com.allen.coding.common.gateway;

import org.springframework.boot.builder.SpringApplicationBuilder;

import com.allen.coding.common.gateway.config.Config;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:19:00
 */
public class GatewayApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(Config.class).web(true).run(args);
	}

}
