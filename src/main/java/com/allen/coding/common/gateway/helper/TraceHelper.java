package com.allen.coding.common.gateway.helper;

import java.util.UUID;

import com.netflix.zuul.context.RequestContext;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:19:59
 */
public class TraceHelper {

	public static final String TRACE_KEY = "trace-id";

	public static String get() {
		Object uuid = RequestContext.getCurrentContext().get(TRACE_KEY);
		if (null == uuid) {
			uuid = UUID.randomUUID().toString().replaceAll("-", "");
			RequestContext.getCurrentContext().set(TRACE_KEY, uuid);
		}
		return uuid.toString();
	}

}
