package com.allen.coding.common.gateway.filter;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.allen.coding.common.gateway.adapter.AccessAdapter;
import com.allen.coding.common.gateway.helper.ContextHelper;
import com.allen.coding.common.gateway.helper.TraceHelper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:19:25
 */
public class AccessFilter extends ZuulFilter {

	private static final Logger LOG = LoggerFactory.getLogger(AccessFilter.class);

	@Override
	public boolean shouldFilter() {
		// true表示让该拦截器生效 false则不生效
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		String token = request.getHeader("access-token");
		if (StringUtils.isBlank(token)) {
			LOG.warn("[{}] apigateway-request-url:{}, apigateway-request-method:{}, token:{}, token is null or empty",
					TraceHelper.get(), request.getRequestURL(), request.getMethod(), token);
			unauthorized(ctx);
			return null;
		}
		AccessAdapter accessAdapter = ContextHelper.getBean(AccessAdapter.class);
		if (accessAdapter.valid(token)) {
			LOG.info(
					"[{}] apigateway-request-url:{}, apigateway-request-method:{}, token:{}, token verification success",
					TraceHelper.get(), request.getRequestURL(), request.getMethod(), token);
		} else {
			LOG.info(
					"[{}] apigateway-request-url:{}, apigateway-request-method:{}, token:{}, token verification failed",
					TraceHelper.get(), request.getRequestURL(), request.getMethod(), token);
			unauthorized(ctx);
		}
		return null;
	}

	@Override
	public String filterType() {
		// pre 在请求被路由之前被调用
		// route 在请求被路由的时候被调用
		// post 在请求被路由之后被调用
		// error 处理请求发生错误时调用
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 0;
	}

	private void unauthorized(RequestContext ctx) {
		ctx.getResponse().setStatus(HttpStatus.UNAUTHORIZED.value());
		ctx.getResponse().setContentType("application/json;charset=UTF-8");
		ctx.setResponseBody("{\"code\":401,\"msg\":\"Unauthorized\"}");
		ctx.setSendZuulResponse(false);
	}

}
