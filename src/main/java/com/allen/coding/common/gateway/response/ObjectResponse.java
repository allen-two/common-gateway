package com.allen.coding.common.gateway.response;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:20:39
 */
public class ObjectResponse<T> {

	private Integer code = 200;

	private String msg = "OK";

	private T data;

	public ObjectResponse() {

	}

	public ObjectResponse(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public ObjectResponse(T data) {
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
