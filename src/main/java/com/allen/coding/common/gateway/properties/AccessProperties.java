package com.allen.coding.common.gateway.properties;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:20:31
 */
@ConfigurationProperties("access")
public class AccessProperties {

	private String gateway;

	private String service;

	private String validatePath;

	public String getGateway() {
		return gateway;
	}

	public String getService() {
		return service;
	}

	public String getValidatePath() {
		return validatePath;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public void setService(String service) {
		this.service = service;
	}

	public void setValidatePath(String validatePath) {
		this.validatePath = validatePath;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
