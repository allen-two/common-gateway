package com.allen.coding.common.gateway.filter;

import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriUtils;

import com.netflix.zuul.context.RequestContext;

/**
 * 定义此类为了解决Spring自带的ProxyRequestHelper
 * buildZuulRequestURI方法会用ISO-8859-1对contextURI编码导致URL乱码的问题
 * 
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:19:40
 */
public class CharsetFilter extends ProxyRequestHelper {

	private static final Logger LOG = LoggerFactory.getLogger(CharsetFilter.class);

	/**
	 * 调用父类的原有逻辑，最后对header中的X-Span-Name进行UTF-8编码
	 */
	@Override
	public MultiValueMap<String, String> buildZuulRequestHeaders(HttpServletRequest request) {
		MultiValueMap<String, String> headers = super.buildZuulRequestHeaders(request);
		List<String> xSpanNameListOrg = headers.get("X-Span-Name");
		List<String> xSpanNameList = new LinkedList<String>();
		if (xSpanNameListOrg != null) {
			for (String xSpanName : xSpanNameListOrg) {
				try {
					xSpanName = UriUtils.encodePath(xSpanName, "UTF-8");
					xSpanNameList.add(xSpanName);
				} catch (UnsupportedEncodingException e) {
					LOG.debug("unable to encode uri path from context, falling back to uri from request", e);
				}
			}
			headers.put("X-Span-Name", xSpanNameList);
		}
		return headers;
	}

	/**
	 * 重写buildZuulRequestURI，不对contextURI编码
	 */
	@Override
	public String buildZuulRequestURI(HttpServletRequest request) {
		RequestContext context = RequestContext.getCurrentContext();
		String uri = request.getRequestURI();
		String contextURI = (String) context.get("requestURI");
		if (contextURI != null) {
			try {
				uri = UriUtils.encodePath(contextURI, "UTF-8");
			} catch (Exception e) {
				LOG.debug("unable to encode uri path from context, falling back to uri from request", e);
			}
		}
		return uri;
	}

}
