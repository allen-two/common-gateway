package com.allen.coding.common.gateway.adapter;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.allen.coding.common.gateway.properties.AccessProperties;
import com.google.gson.Gson;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:19:07
 */
@Component
@EnableConfigurationProperties({ AccessProperties.class })
public class AccessAdapter {

	@Autowired
	private AccessProperties accessProperties;
	@Autowired
	private RestTemplate restTemplate;

	public boolean valid(String token) {
		Map<String, Object> tokenMap = new HashMap<String, Object>();
		tokenMap.put("token", token);
		StringBuffer sbUrl = new StringBuffer("http://");
		if (StringUtils.isNotEmpty(accessProperties.getGateway())) {
			sbUrl.append(accessProperties.getGateway()).append("/");
		}
		sbUrl.append(accessProperties.getService()).append(accessProperties.getValidatePath());
		String entity = restTemplate.getForObject(sbUrl.toString(), String.class, tokenMap);
		Gson gson = new Gson();
		@SuppressWarnings("unchecked")
		Map<String, Object> gsomMap = gson.fromJson(entity, Map.class);
		String code = (String) gsomMap.get("code");
		return "200".equals(code) && (gsomMap.get("data") != null);
	}

}
