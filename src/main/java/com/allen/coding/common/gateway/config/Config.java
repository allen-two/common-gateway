package com.allen.coding.common.gateway.config;

import com.allen.coding.common.gateway.filter.AccessFilter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonCommandFactory;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonRoutingFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.allen.coding.common.gateway.filter.CharsetFilter;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:19:15
 */
@Configuration
@SpringBootApplication
@RestController
@EnableZuulProxy
@ComponentScan({ "com.allen.coding.common.gateway" })
public class Config {

	@LoadBalanced
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/**
	 * 解决编码问题
	 * 
	 * @param zuulProperties
	 * @return
	 */
	@Bean
	public ProxyRequestHelper proxyRequestHelper(ZuulProperties zuulProperties) {
		ProxyRequestHelper helper = new CharsetFilter();
		helper.setIgnoredHeaders(zuulProperties.getIgnoredHeaders());
		helper.setTraceRequestBody(zuulProperties.isTraceRequestBody());
		return helper;
	}

	/**
	 * 覆盖掉ZuulProxyConfiguration中定义的ribbonRoutingFilter
	 */
	@Bean
	public RibbonRoutingFilter ribbonRoutingFilter(ProxyRequestHelper helper,
			RibbonCommandFactory<?> ribbonCommandFactory) {
		RibbonRoutingFilter filter = new RibbonRoutingFilter(helper, ribbonCommandFactory);
		return filter;
	}

	/**
	 * 认证Filter，当spring.profiles.active=ACCESS时生效
	 * 
	 * @return
	 */
	@Bean
	@Profile("ACCESS")
	public AccessFilter accessFilter() {
		return new AccessFilter();
	}

	/**
	 * 跨域控制，当spring.profiles.active=CORS时生效
	 */
	@Bean
	@Profile("CORS")
	public CorsFilter corsFilter() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		final CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PATCH");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

}
