package com.allen.coding.common.gateway.helper;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @author Allen ZHANG
 * @date 2018年7月7日 下午8:19:51
 */
@Component
public class ContextHelper implements ApplicationContextAware {

	private static ApplicationContext applicationContext;

	public static ApplicationContext getSpringContext() {
		return applicationContext;
	}

	public static <T> T getBean(Class<T> clazz) {
		return applicationContext.getBean(clazz);
	}

	public static Object getBean(String beanKey) {
		return applicationContext.getBean(beanKey);
	}

	public static ApplicationContext getWebSpringContext(HttpServletRequest request) {
		return getWebSpringContext(request.getSession());
	}

	public static ApplicationContext getWebSpringContext(HttpSession session) {
		return getWebSpringContext(session.getServletContext());
	}

	public static ApplicationContext getWebSpringContext(ServletContext servletContext) {
		return WebApplicationContextUtils.getWebApplicationContext(servletContext);
	}

	public static Object getWebBean(HttpServletRequest request, String beanKey) {
		return getWebSpringContext(request).getBean(beanKey);
	}

	public static Object getWebBean(HttpSession session, String beanKey) {
		return getWebSpringContext(session).getBean(beanKey);
	}

	public static Object getWebBean(ServletContext servletContext, String beanKey) {
		return getWebSpringContext(servletContext).getBean(beanKey);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ContextHelper.applicationContext = applicationContext;
	}
}